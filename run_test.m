function run_test()

addpath './'
addpath './features-speakers/'

% Path to the trained data
datapath = '/home/mdiazmel/data/face_recog/';

% Path to matconvnet installation
matconvnetpath = '/home/mdiazmel/code/matconvnet-1.0-beta25/matlab/';


% Loading data for face detection and recognition
[face_model, convNet, feature, gt2, mean_face] = model_preloading(datapath, matconvnetpath);

% Test image
img = imread([datapath '/holo_photo.jpg']);

% Face detection parameters
detection_threshold = 0.2;
% 0.3 or 0.2 are adequate for face detection.
nms_threshold = 0.3;
t = tic;
[det, ~] = cv_face_detec(img, face_model.model, detection_threshold, nms_threshold);
t1 = toc(t);

% Face recognition function
t = tic;
[det_coord names] = cv_face_recog(img, det, convNet, feature, gt2, mean_face);
t2 = toc(t);

fprintf(['Processing time detection: %4.3f \n' ...
        'Processing time recognition: %4.3f \n' ...
        'Total processing time: %4.3f \n'], t1, t2, t1+t2);

if ~isempty(det_coord)
  bboxes = [det_coord(1) det_coord(2) ...
  det_coord(3)-det_coord(1) ...
  det_coord(4)-det_coord(2)];
else
  fRect = [size(img,1)/3 size(img,2)/3 size(img,1)/3 size(img,1)/3];
  fRect = round(fRect);
  bboxes(1,:) = fRect;
end

