function [face_model,convNet,features,gt2,mean_face] = model_preloading(datapath, matconvnetpath)

global FR_DATAPATH;
FR_DATAPATH = datapath;
addpath(matconvnetpath);
vl_setupnn;

%addpath('/home/iroccosp/yolomex');
%addpath '/home/amiech/hololens/matconvnet-1.0-beta20/matlab';
%addpath '/home/amiech/hololens/matconvnet-1.0-beta20/matlab/simplenn';
fprintf('Loading vgg CNN...\n');

gpuDevice(3);

config.paths.net_path = [FR_DATAPATH 'vgg_face.mat'];

convNet = lib.face_feats.convNet(config.paths.net_path);
convNet.net = vl_simplenn_move(convNet.net,'gpu');

model_path = [FR_DATAPATH 'dpm_baseline.mat'];
face_model = load(model_path);

load([FR_DATAPATH 'features.mat']);
load([FR_DATAPATH 'mean_face.mat']);

%irroco_path = '/home/iroccosp/yolomex';
%namefile = fullfile(irroco_path,'facerec/fddb.names');
%
%cfgfile = fullfile(irroco_path,'facerec/yolo-fddb.cfg');
%weightfile = fullfile(irroco_path,'facerec/yolo-fddb_v2.weights');
%filename = fullfile(irroco_path,'facerec/france.jpg');
%thresh = 0.24;
%hier_thresh = 0.5;
%
%gpuDevice(1);
%yolo_gpu_device = 1;
%yolomex('init_name',namefile,cfgfile,weightfile,yolo_gpu_device);
%
%display('DONE!  yolo model has been loaded');
