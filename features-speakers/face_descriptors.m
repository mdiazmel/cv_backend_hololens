function features = face_descriptors(Im, det, mean_face, convNet)

DEBUG = 0;

imsz = 75;
if size(Im, 3)==1
   Im = repmat(Im, [1 1 3]);
end

det = det([1,3,2,4,5,6]);

pose_cor = [3 2 2 3 1 1];

pose    = pose_cor(det(5));
t = tic;
P = get_landmarks2(Im,det);
t1 = toc(t);
t = tic;
K = warp_face(Im, mean_face{pose}, P, imsz );
t2 = toc(t);

if pose == 2
    K = K(:,end:-1:1,:);
end

K = K(1:end-1,1:end-1,:);

fprintf('compute vgg descriptors ...\n');
t = tic;
features = squeeze(convNet.simpleNN(K));
t3 = toc(t);

features = features/norm(features);

if (DEBUG == 1)
  fprintf(['Time elapsed function get_landmarks2: %4.3f \n' ...
  'Time elapsed function warp_face: %4.3f \n' ...
  'Time elapsed function squeeze: %4.3f \n'], t1, t2, t3);
   
end


