function [ fd ] = face_descriptors( fd , mean_face , imsz , dumpfile, model_dir )
%FACE_DESCRIPTORS Extracts facial descriptors for faces fd
%  This function takes as input face detections with landmark positions
%  already computed. It warps the face using a similarity transfor so that
%  landmarks positions correspond to the mean positions in the complete
%  video. Once the warping is done it recomputes landmark positions and
%  describe them using sift descriptors.
%
%       The input arguments are : 
%               - fd        : the face-detection structure used everywhere
%               - mean_face : the average face landmarks location
%               - imsz      : the size of the face after warping (101 px)
%               - debug     : if 1 will show landmark detections
%
%       The resulting face descriptors are added to the fd
%       structure. The following fields are created : 
%               - ??????????????????????
%fprintf('Loading vgg CNN...\n');
%config.paths.net_path = 'data/vgg_face.mat';

%convNet = lib.face_feats.convNet(config.paths.net_path);

fd().dSIFT = [];

frame = cat(1,fd.frame);

f = unique(frame);
f = f(f > 4000 & f < 7200);
permutation = randperm(length(f));
c = 0
fprintf('Extracting Face Descriptors...\n');
figure(1)
for i = permutation(1:100)

    
    %impath = char(dumpfile(i));
    fprintf('working on frame %06d..., c = %d \n',f(i),c);
    impath = sprintf(dumpfile, f(i));
    Im = imread(impath);

    if size(Im, 3)==1
        Im = repmat(Im, [1 1 3]);
    end
    
    idx = find( frame == f(i) );

    for j = 1:length(idx)
        k = idx(j);
        
        pose    = fd(k).pose;
        P       = fd(k).P;
        
        % getting rectified image patch
        % K is centered on the face, of size [3*IMSZ x 3*IMSZ]
        % the face region is in the middle, of size [IMSZ x IMSZ]
        K = warp_face(Im, mean_face{pose}, P, imsz );
        
      
       imshow(K);
       pause(1);
        % getting the face descriptors in the rectified image
        %siftVec = describe_face(K, pose, model_dir);
        %K = K(1:end-1,1:end-1,:);
	%   siftVec = convNet.simpleNN(K);

        %fd(k).dSIFT = double(siftVec(:));
        
    end
    
    %if mod(c,5000) == 0 
    %    fprintf('temp saving... \n',i);
    %    save('facedets_tmp_130000.mat','fd');
    %end
    %c = c+1;
end
fprintf('Done.\n');
    
end


