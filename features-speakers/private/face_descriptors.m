function [ fd ] = face_descriptors( fd ,result_dir, mean_face , imsz , dumpfile, model_dir, convNet,thresh_pconf,thresh_trackconf)
%FACE_DESCRIPTORS Extracts facial descriptors for faces fd
%  This function takes as input face detections with landmark positions
%  already computed. It warps the face using a similarity transfor so that
%  landmarks positions correspond to the mean positions in the complete
%  video. Once the warping is done it recomputes landmark positions and
%  describe them using sift descriptors.
%
%       The input arguments are : 
%               - fd        : the face-detection structure used everywhere
%               - mean_face : the average face landmarks location
%               - imsz      : the size of the face after warping (101 px)
%               - debug     : if 1 will show landmark detections
%
%       The resulting face descriptors are added to the fd
%       structure. The following fields are created : 
%               - ??????????????????????
fd().descriptors = [];

fprintf('Pre-processing..\n');

%{
%thresh trackconf 
trackconf = [fd.trackconf];
mask = trackconf > thresh_trackconf;
fd = fd(mask);

%thresh average pconf
tracks = [fd.track];
utracks = unique(tracks);

pconf = [fd.pconf];
average_pconf = grpstats(pconf,tracks,'mean');
mask = average_pconf > thresh_pconf;
mask = ismember(tracks,utracks(mask));
fd = fd(mask);
%}
tracks = [fd.track];
utracks = unique(tracks);

%keep top5 best pconf:
pconf = [fd.pconf];
tracks = [fd.track];
utracks = unique(tracks);
mask = zeros(1,length(pconf));
for i=1:size(utracks,2)
    ind = tracks == utracks(i);
    fd_i = fd(find(ind));
    pconf_i = [fd_i.pconf];

    [value,indice] = sort(pconf_i,'descend');
    n = length(indice);

    track_pconf_threshold = value(min(5,n));
    mask = mask | (ind & ( pconf < track_pconf_threshold));
end
fd(mask) = [];



length(utracks);

frame = cat(1,fd.frame);

f = unique(frame);
size(f)
dumpfile
fullfile(result_dir,'facedets_vgg.mat')
batchSize = 100; 
fprintf('Extracting Face Descriptors...\n');
c=1;
K_batch = [];
ind_batch = [];

load(fullfile(model_dir,'pose_cor_eccv2014.mat'));
for i = f'
    
    %impath = char(dumpfile(i));
    fprintf('working on frame %06d..., c = %d \n',i,c);
    impath = sprintf(dumpfile, i);
    %impath = char(dumpfile(i));
    Im = imread(impath);

    if size(Im, 3)==1
        Im = repmat(Im, [1 1 3]);
    end
    
    idx = find( frame == i );

    for j = 1:length(idx)
        k = idx(j);
        
        pose    = fd(k).pose;

	    %pose    = fd(k).pose;
        P       = fd(k).P;

        %if pose > 1
        %    P = P(:,1:5);
        %end
        
        % getting rectified image patch
        % K is centered on the face, of size [3*IMSZ x 3*IMSZ]
        % the face region is in the middle, of size [IMSZ x IMSZ]
        %K = warp_face(Im, mean_face{pose}, P, imsz );
        K = warp_face(Im, mean_face{pose}, P, imsz );


        if pose == 2
            K = K(:,end:-1:1,:);
	    end

        K = K(1:end-1,1:end-1,:);

        K_batch = cat(4,K_batch,K);

        ind_batch = cat(1,ind_batch,k);


        if size(K_batch,4) == batchSize
            tic;
  	    siftVec_batch = squeeze(convNet.simpleNN(K_batch));
            toc;
            size(siftVec_batch)
            for l = 1:size(ind_batch,1)
                v = siftVec_batch(:,l);
	        %v = v/norm(v);
                fd(ind_batch(l)).descriptors = double(v);  
            end
	        K_batch = [];
            ind_batch = [];
        end
        
    end


    c = c+1;
end

if size(K_batch,4) > 1
	siftVec_batch = squeeze(convNet.simpleNN(K_batch));

	for l = 1:size(ind_batch,1)
	   v = siftVec_batch(:,l);
	   %v = v/norm(v);
	   fd(ind_batch(l)).descriptors = double(v);  

	end
end

fprintf('final saving... \n',i);
facedets = fd;
%save(fullfile(result_dir,'facedets_buffy_02_vgg.mat'),'facedets');

save(result_dir,'facedets');
fprintf('Done.\n');
    
end


