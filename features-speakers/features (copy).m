function features(result_dir, model_dir, dump_string)

data = load(fullfile(result_dir, 'facedets_lite_landmarks.mat'));
load(fullfile(model_dir, 'mean_face.mat'));
addpath '../matconvnet-1.0-beta20';

addpath '../matconvnet-1.0-beta20/matlab';
vl_setupnn;

fprintf('Loading vgg CNN...\n');
config.paths.net_path = 'data/vgg_face.mat';

convNet = lib.face_feats.convNet(config.paths.net_path);
convNet.net = vl_simplenn_move(convNet.net,'gpu');

facedets = face_descriptors(data.facedets, mean_face, 75, dump_string, model_dir,convNet,-30,0.6);

%save(fullfile(result_dir, 'facedets_vgg.mat'));

end
